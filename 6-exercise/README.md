# Answers to the sixth tutorial quiz

### 1. What types of proxies does the Apify Proxy include? What are the main differences between them?

> 1. Datacenter proxy – it uses data centers to mask your IP address. Chance of blocking due to other users' activity.
> 2. Residential proxy – IP addresses located in homes and offices around the world. These IPs have the lowest chance of blocking.
> 3. Google SERP proxy – download and extract data from Google Search engine result pages (SERPs). You can select country and language to get localized results.

### 2. Which proxies (proxy groups) can users access with the Apify Proxy trial? How long does this trial last?

> _It is a datacenter proxies._

> _Free trial period of Apify Proxy is 30 days_

### 3. How can you prevent a problem that one of the hardcoded proxy groups that a user is using stops working (a problem with a provider)? What should be the best practices?

> _You can prevent a blocking of proxy by SessionPool, that filters out blocked or non-working proxies, so your actor does not retry requests over known blocked proxies._

### 4. Does it make sense to rotate proxies when you are logged in?

> _No, because it will be some strange, when default user changes his ip address. Default user never logs in from one ip and changes it to another, so it will be looking some strange for site. Very rarely, when site will block the IP but not the account so you have to rotate your proxy._

### 5. Construct a proxy URL that will select proxies only from the US (without specific groups).

> _http://\<session>,country-US:\<password>@proxy.apify.com:8000_

### 6. What do you need to do to rotate proxies (one proxy usually has one IP)? How does this differ for Cheerio Scraper and Puppeteer Scraper?

> _For each request, a random IP address is chosen from all available proxy groups. You can use random IP addresses from proxy groups by specifying the group(s) in the username parameter. A random IP address will be used for each request._

> _Puppeteer rotates proxy/IP only after the browser changes and use the same IP for 100 requests, unlike Cheerio_

### 7. Try to set up the Apify Proxy (using any group or auto) in your browser. This is useful for testing how websites behave with proxies from specific countries (although most are from the US). You can try Switchy Omega extension but there are many more. Were you successful?

> _I set up Apify Proxy using Datacenter proxy. Also, I tried to use Switchy Omega._

### 8. Name a few different ways a website can prevent you from scraping it.

> 1. Behavior tracking;
> 2. Browser fingerprinting;
> 3. Block IP address;
> 4. Requests settings(headers, user agents, cookies, tokens);

### 9. Do you know any software companies that develop anti-scraping solutions? Have you ever encountered them on a website?

> _No, i don't._
