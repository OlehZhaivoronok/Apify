# Answers to the fourth tutorial quiz

### 1. Do you have to rebuild an actor each time the source code is changed?

> _Yes, we have. Because we need to run a new build with new changes._

### 2. What is the difference between pushing your code changes and creating a pull request?

> _When we create a pull request we merge changes between two different branches, can see differences and other info. Also, pull request summarizing the changes between difference branches._

### 3. How does the apify push command work? Is it worth to using, in your opinion?

> _Push command uploads the actor to the Apify platform and builds it there. Yes of course, it is useful when we create our actor locally and need to push it to the Apify platform._
