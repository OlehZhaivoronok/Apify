# Answers to the first advanced tutorial quiz

### 1. Do you need to build public actors to use them?

> _No. When you use public actors, you run it._

### 2. How would you call an actor with a specific version?

> _Call an actor via Apify CLI, using command **apify call** and provide a version in the settings file._

> _Run an actor via Apify API, using post request **https://api.apify.com/v2/acts/actorId/runs** with parameter **build** which specifies the actor build to run._

> _Run an actor via Apify SDK, using method **Apify.call(actId, [input], [options])** and put **build** parameter in options object._

### 3. Do you require any specific proxy to use the Google Search Scraper? If yes, how does it work?

> _To use this actor, you'll need access to Apify Proxy and need to have a sufficient limit for Google SERP queries. Google SERP proxy – download and extract data from Google Search engine result pages._

### 4. How can you set up a country to scrape from in Google Search Scraper?

> _You have to put a country parameter to INPUT. By default, the actor uses United States._

### 5. What type of authentication/authorization does the Google Sheets actor use? What are its benefits?

> _We have to login with the same Google account where the spreadsheet locate, authorize and allow Apify to work with your spreadsheets. Internally they use their npm package apify-google-auth. This small library allows using OAuth for Google services with Apify actors platform. After you authorize for the first time, tokens store in your key-value store (option tokensStore which is by default google-oauth-tokens), and you don't need to authorize again. So after the first usage, you can fully automate the actor. _
