# Answers to the fifth tutorial quiz

### 1. What is the relationship between actor and task?

> _Actor tasks is like a configuration for your actor. You can configure specific parameters, inputs and run actor with it._

### 2. What are the differences between default (unnamed) and named storage? Which one would you choose for everyday usage?

> _Unnamed storages expire after 7 days and not take up your storage space. Named storages retained indefinitely.xs_

> _If you want to retain some data, you have to select named storage. In use cases when it isn't required, you have to select the unnamed storage._

### 3. What is the relationship between the Apify API and the Apify client? Are there any significant differences?

> _Apify Api is a REST api, Apify client is an implementation of this API and it implements exponential backoff which helps with random API outages._

### 4. Is it possible to use a request queue for deduplication of product IDs? If yes, how would you do that?

> _You have to go to **Check for duplicates** section_, choose the **Enable duplicate items check** option and set the **Unique keys** field to id of your row.

### 5. What is data retention and how does it work for all types of storage (default and named)?

> _Unnamed storages expire after 7 days. Named storages retained indefinitely._

### 6. How do you pass input when running an actor or task via the API?

> _You have to use POST request with the payload property, which will be the input of the runing actor._
