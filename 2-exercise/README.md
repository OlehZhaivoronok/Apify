# Answers to the second tutorial quiz

### 1. Where and how can you use JQuery with the SDK?

> _Within Cheerio you get **$** property in handlePageFunction (). Within this property you can work with DOM of HTML page, but Cheerio doesn’t have full JQuery API. Within Puppeteer you have a function to do that: **Apify.utils.puppeteer.injectJQuery**_ and you can use jQuery inside **page.evaluate**.

### 2. What is the main difference between Cheerio and JQuery?

> _Cheerio for Node.js. jQuery for a browser. Cheerio doesn’t have full JQuery API._

### 3. When would you use CheerioCrawler and what are its limitations?

> _It is a good idea when you need to do high workloads._

> _But most websites nowadays use JavaScript to web sites, and Cheerio doesn't have a ability to crawl that sites it can only use the plain http request._

### 4. What are the main classes for managing requests and when and why would you use one instead of another?

> _The main classes are RequestList and RequestQueue. RequestList is static, immutable list of URLs. RequestQueue represents a dynamic queue of Requests. It can be updated at runtime by adding more pages. Use Request list when you have pre-existing list of your own URLs. Use RequestQueue when you need to add URLs dynamically._

### 5. How can you extract data from a page in Puppeteer without using JQuery?

> _handlePageFunction has a **page** property. And, you have ability to get data from page by this property by selectors._

### 6. What is the default concurrency/parallelism the SDK uses?

> _minConcurrency scale up automatically if you hadn't set this parameter before._

> _maxConcurrency default value equal minConcurrency_

> _desiredConcurrency default value equal minConcurrency_

> _desiredConcurrency default value equal minConcurrency_
