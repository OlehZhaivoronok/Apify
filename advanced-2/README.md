# Answers to the second advanced tutorial quiz

### 1. Do you have idea what is the general country distribution of Apify proxies? You can do a small test on any IP locator.

> _Most from US._

### 2. Why using the country parameter in the proxy configuration is not good enough?.

> _There can be an infinite amount of these databases and any website can implement it as it wishes._

> _Apify Proxy uses just one of these databases for its country parameter and as a special case, for Google domain, it uses Google localizations that are vastly different from the main database._

### 3. Do you think it is a good idea to hardcode single cookies into the code to solve country localization?.

> _This is the best approach if possible. Because sometimes there is no single cookie that you could extract and hardcode._
