module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
    },
    extends: '@apify',
    parserOptions: {
        ecmaVersion: 12,
    },
    rules: {
        indent: ['error', 4, { SwitchCase: 1 }],
        'linebreak-style': ['error', 'unix'],
        semi: ['error', 'always'],
    },
};
