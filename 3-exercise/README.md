# Answears to the third tutorial quiz

### 1. How do you allocate more CPU for your actor run?

> _The amount of CPU is calculated automatically from the memory. So for allocate more CPU you have to allocate more MB of memory_

### 2. How can you get the exact time when the actor was started from within the running actor process?

> _You can do it by a startedAt env variable._

### 3. Which are the default storages an actor run is allocated (connected to)?

> _Key-value store, dataset, request queue._

### 4. Can you change the memory allocated to a running actor?

> _No. When invoking the actor, the caller has to specify the amount of memory allocated for the actor._

### 5. How can you run an actor with Puppeteer in a headful (non-headless) mode?

> _You have to use **Node.js 12 + Chrome + Xvfb on Debian** docker image. It opens non-headless Chrome._

### 6. Imagine the server/instance the container is running on has a 32 GB, 8-core CPU. What would be the most performant (speed/cost) memory allocation for CheerioCrawler? (Hint: NodeJS processes cannot use user-created threads)

> _If you have to scrape a single simple page, you have to set up memory to 128 - 512 MB for Cheerio, 1024 MB for Puppeteer._

> _For more difficult jobs, you have to increase memory and CPU's amount will be increased automatically. I would test it with 1024 - 4096 MB for Cheerio and 4096 - 16384 MB for Puppeteer._

### 7. What is the difference between RUN and CMD Dockerfile commands?

> _RUN is an image build step, the state of the container after a RUN command will be committed to the container image. A Dockerfile can have many RUN steps that layer on top of one another to build the image._

> _CMD is the command the container executes by default when you launch the built image. A Dockerfile will only use the final CMD defined. The CMD can be overridden when starting a container with docker run $image $other*command.*_

### 8. How does the FROM command work and which base images Apify provides?

> _FROM command provide the image which will be the base image of created container._

> Base Apify images:
>
> 1.  Node.js 12 on Alpine Linux (apify/actor-node-basic)
> 2.  Node.js 12 + Chrome on Debian (apify/actor-node-chrome)
> 3.  Node.js 12 + Chrome + Xvfb on Debian (apify actor-node-chrome-xvfb)
